"use strict";

const _ = require("lodash");

module.exports = {
    runTests
};

let allPass = true;

function runTests(testCases, solution) {
    let onlyOne = _.find(testCases, (item) => item.only);

    if (onlyOne) {
        runSingleTest(onlyOne, solution);

        if (allPass) {
            console.log("Single test is OK!");
            console.log("WARNING: enable all tests for final check!");
        }
    } else {
        testCases.forEach((testCase) => {
            runSingleTest(testCase, solution);
        });

        if (allPass) console.log("All tests are OK!");
    }
}

function runSingleTest(testCase, solution) {
    let params = _.isArray(testCase.input) ? testCase.input : [testCase.input];

    let result = solution.apply(this, params);

    if (!areEqual(result, testCase.result)) {
        console.log(`Test fails for input: ${testCase.input}`);
        console.log(`Actual: ${result}; Expected: ${testCase.result}`);
        allPass = false;
    }
}

function areEqual(actual, expected) {
    if (_.isArray(actual)) {
        return compareArrays(actual, expected)
    } else if (_.isObject(actual)) {
        return compareObjects(actual, expected)
    }

    return actual === expected;
}

function compareArrays(actual, expected) {
    if (actual.length != expected.length) return false;
    for (let index = 0; index < actual.length; index++) {
        if (actual[index] !== expected[index]) return false;
    }

    return true;
}

function compareObjects(actual, expected) {
    for (var p in actual) {
        if (actual.hasOwnProperty(p)) {
            if (actual[p] !== expected[p]) {
                return false;
            }
        }
    }

    for (var p in expected) {
        if (expected.hasOwnProperty(p)) {
            if (actual[p] !== expected[p]) {
                return false;
            }
        }
    }

    return true;
}