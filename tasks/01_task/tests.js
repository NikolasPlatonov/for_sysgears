module.exports = [
  {
    input: "26C",
    result: {K: 299, F: 79}
  },
  // {
  //   input: "26C",
  //   result: {"K": "299", "F": "79"}
  // },
  // {
  //   input: "299K",
  //   result: {"C": "26", "F": "79"}
  // },
  // {
  //   input: "79F",
  //   result: {"K": "299", "C": "26"}
  // }
];
