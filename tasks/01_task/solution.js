module.exports = {
  tests: require('./tests.js'),
  solution
};

function solution(input) {

  let tempType = input.toString().slice(-1);

  let valueTemp = (input) => {

    let value = parseInt(input);
    let valueC = value;
    let valueK = value;
    let valueF = value;

    if (tempType === "C") {
      return (valueC);
    } else if (tempType === "K") {
      return (valueK);
    } else if (tempType === "F") {
      return (valueF);
    }
  }

  if (tempType === "C") {
    let Kc = Math.ceil(valueTemp(input) + 273);
    let Fc = Math.ceil(valueTemp(input) * 1.8 + 32);
    let resultFromC = {
      K: Kc,
      F: Fc
    }
    // console.log(resultFromC);
    return (resultFromC);

  } else if (tempType === "K") {
    let Ck = Math.ceil(valueTemp(input) - 273);
    let Fk = Math.ceil(valueTemp(input) * 1.8 - 460);
    let resultFromK = {
      C: Ck,
      F: Fk
    }
    // console.log(resultFromK);
    return (resultFromK);

  } else if (tempType === "F") {
    let Kf = Math.round((valueTemp(input) + 460) / 1.8);
    let Cf = Math.round((valueTemp(input) - 32) / 1.8);
    let resultFromF = {
      K: Kf,
      C: Cf
    }
    // console.log(resultFromF);
    return (resultFromF);
  }
}
