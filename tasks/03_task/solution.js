module.exports = {
  tests: require('./tests.js'),
  solution
};

function solution(chanceForA, chanceForB, startName) {

  let first = chanceForA;
  let second = chanceForB;

  let player = startName;
  let n = 20 / 2;

  let prob = [];
  prob.push(first, second);

  let resProb = [];
  resProb.push(((1 - first) / n), ((1 - second) / n));

  if (player === "A") {
    for (let i = 0; i <= n - 1; i++) {
      // console.log("if the A goes first: ", ((resProb[0] * i) + prob[0]), "  >  ", ((1 - resProb[1] * (i + 1)) + prob[1]));
      if (((resProb[0] * i) + prob[0]) > ((1 - resProb[1] * (i + 1)) + prob[1])) {
        // console.log("The optimal shot step is : " + i);
        return (i);
      }
    }
  } else {
    if (prob[1] > (1 - resProb[0] - prob[0])) {
      // console.log("if the B shoots at the first step: ", prob[1], "  >  ", (1 - resProb[0] - prob[0]));
      // console.log("The optimal shot step is : " + n - 1);
      return (n = n - 1);
    } else {
      for (let i = 0; i <= n - 2; i++) {
        // console.log("like the first variant: ", ((resProb[0] * i) + prob[0]), ">", ((1 - resProb[1] * (i + 2) - prob[1])));
        if (((resProb[0] * i) + prob[0]) > ((1 - resProb[1] * (i + 2) - prob[1]))) {
          // console.log("The optimal shot range is: " + i);
          return i;
        }
      }
    }
  }
}