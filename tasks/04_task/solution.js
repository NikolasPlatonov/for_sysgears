module.exports = {
  tests: require('./tests.js'),
  solution
};

function solution() {

  let startTravelPoints = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  let listOfDrivers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  let driversMoney = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  function travelPoints(arr) {
    let sum = 0;
    for (let i = 0; i < arr.length; i++) {
      arr[i] = Math.floor(1 + Math.random() * 10);
      sum += arr[i];
      if (i == 9 && sum <= 55) {
        travelPoints(arr)
      }
    }
    return arr;
  }

  let fare = travelPoints(startTravelPoints);
  let sum = fare.reduce(function (a, b) {return a + b;}, 0);

  // function fareEqualCoinsNominal(a, b) {
  //   let aNew = [];
  //   for (let i = 0; i < a.length; i++) {
  //     for (let j = 0; j < b.length; j++) {
  //       if (a[i] == b[j]) {
  //         aNew.push(a[i]);
  //         break;
  //       }
  //     }
  //   }
  //   return aNew;
  // }

  let fareEqualCoinsNom = driversMoney.filter(function (n) {
    return fare.indexOf(n) >= 0;
  });

  let unUsedCoins = driversMoney.filter(function (n) {
    return fare.indexOf(n) < 0;
  });

  // let unUsedTravelPoints = fare.filter(x => !fareEqualCoinsNom.includes(x));
  // let unUsedTravelPoints = fare.filter(a => fareEqualCoinsNom.some(b => a === b));

  function diff(a1, a2) {
    return a1.concat(a2).filter(function (val, index, arr) {
      return arr.indexOf(val) === arr.lastIndexOf(val);
    });
  }

  console.log("DIF", diff(fare, fareEqualCoinsNom));

  console.log("FARE", fare);
  console.log("SUM", sum);
  // console.log("USED_COINS_V1", fareEqualCoinsNominal(driversMoney, fare));
  console.log("USED_COINS_V2", fareEqualCoinsNom);
  console.log("UNUSED_COINS", unUsedCoins);
  // console.log("UNUSED_POINTS", unUsedTravelPoints);

  // console.log("fareEqualCoinsNominal", fareEqualCoinsNominal(driversMoney, fare));
  // return (travelPoints(startTravelPoints)).sort(function (a, b) {return a - b;});
  // return (travelPoints(startTravelPoints));
  // return (fareEqualCoinsNominal(driversMoney, fare));
  return (fare);

}

