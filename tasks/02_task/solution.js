module.exports = {
  tests: require('./tests.js'),
  solution
};

function solution(plate, truckA, truckC, placeB) {

  var result = [];

  platesUpLoading(plate, truckA, truckC, placeB, result);
  // console.log('RESULT:', result);

  // call the function to truckA solving the task for 8 plates
  function platesUpLoading(plate, truckA, truckC, placeB, result) {
    if (plate == 1) {
      // base case of 1 plate - we know how to solve that))
      // console.log("1 from " + truckA + " to " + truckC);
      result.push("1 from " + truckA + " to " + truckC);

    } else {
      // first solve for 6 plates (i.e., plate - 1)
      platesUpLoading(plate - 1, truckA, placeB, truckC, result);

      // now move the 8th plate
      // console.log(plate + " from " + truckA + " to " + truckC);
      result.push(plate + " from " + truckA + " to " + truckC);

      // now solve for the 7 plates from post B to post C
      platesUpLoading(plate - 1, placeB, truckC, truckA, result);
    }
  }
  return result;
}

///////////////////////////////////////////////////////////////////

// function solution(plates) {

//   solutionUnLoadingTask(plates);

//   function solutionUnLoadingTask(plates) {

//     solutionUnLoadingMethod(plates, 1, 3)
//   }

//   function solutionUnLoadingMethod(plates, truckA, truckC) {

//     let places = "abc";

//     if (plates == 0) return;

//     let placeB = 6 - truckA - truckC;
//     solutionUnLoadingMethod(plates - 1, truckA, placeB);

//     // console.log(plates, places[truckA - 1], "->", places[truckC - 1]);

//     let res1 = plates;
//     let res2 = places[truckA - 1];
//     let res3 = places[truckC - 1];
//     let result = (String(res1 + " : " + res2 + " -> " + res3));

//     console.log(result);

//     solutionUnLoadingMethod(plates - 1, placeB, truckC);

//     return (result);
//   };
// }