module.exports = [
  {
    input: [1, "a", "c", "b"],
    result: [
      "1 from a to c"
    ]
  },
  {
    input: [2, "a", "c", "b"],
    result: [
      "1 from a to b",
      "2 from a to c",
      "1 from b to c",
    ]
  },
  {
    input: [3, "a", "c", "b"],
    result:
      [
        "1 from a to c",
        "2 from a to b",
        "1 from c to b",
        "3 from a to c",
        "1 from b to a",
        "2 from b to c",
        "1 from a to c",
      ]
  }
];
