'use strict';

const testHelper = require('./testHelper');
// const task = require('./tasks/01_task/solution');
// const task = require('./tasks/02_task/solution');
const task = require('./tasks/03_task/solution');
// const task = require('./tasks/04_task/solution');

testHelper.runTests(task.tests, task.solution);
